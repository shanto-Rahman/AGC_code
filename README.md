# AGC_code
This code is developed to automatically correct an images having low illumination variation. Detail is in the paper - Rahman, Shanto, et al. "An adaptive gamma correction for image enhancement." EURASIP Journal on Image and Video Processing 2016.1 (2016): 1-13.
